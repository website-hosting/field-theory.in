"""
WSGI config for field_theory project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os, sys

sys.path.append("/home/field/public_html")
sys.path.append("/home/field/public_html/rootapp")

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'field_theory.settings')
os.environ['PYTHON_EGG_CACHE'] = '/home/field/.python_egg_cache'

application = get_wsgi_application()
