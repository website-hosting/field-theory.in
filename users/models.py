from django.db import models

# Create your models here.

class Group(models.Model):
    name = models.CharField(max_length=128)

class User(models.Model):
    name = models.CharField(max_length=128)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

