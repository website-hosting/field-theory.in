from django.db import models

# Create your models here.

class Book(models.Model):
    name = models.CharField(max_length=128)

class Article(models.Model):
    name = models.CharField(max_length=128)

class Shelf(models.Model):
    name = models.CharField(max_length=128)

class Library(models.Model):
    name = models.CharField(max_length=128)

class Publisher(models.Model):
    name = models.CharField(max_length=128)

class Author(models.Model):
    name = models.CharField(max_length=128)


