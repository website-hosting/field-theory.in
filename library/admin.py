from django.contrib import admin

# Register your models here.

from .models import Book, Library, Shelf, Article, Publisher, Author


admin.site.register(Book)
admin.site.register(Article)
admin.site.register(Shelf)
admin.site.register(Publisher)
admin.site.register(Author)
admin.site.register(Library)
