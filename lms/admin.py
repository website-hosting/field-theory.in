from django.contrib import admin

# Register your models here.

from .models import User, Student, Teacher, Course, Lesson, Topic
from .models import Role, Permission, Fees, Schedule, Duration

admin.site.register(User)
admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(Course)
admin.site.register(Lesson)
admin.site.register(Topic)
admin.site.register(Role)
admin.site.register(Permission)
admin.site.register(Fees)
admin.site.register(Schedule)
admin.site.register(Duration)
