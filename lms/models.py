from django.db import models

# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=128)

class Student(models.Model):
    firstname = models.CharField(max_length=128)
    lastname = models.CharField(max_length=128)

class Teacher(models.Model):
    firstname = models.CharField(max_length=128)
    lastname = models.CharField(max_length=128)

class Course(models.Model):
    name = models.CharField(max_length=128)

class Topic(models.Model):
    name = models.CharField(max_length=128)

class Lesson(models.Model):
    name = models.CharField(max_length=128)

class Role(models.Model):
    name = models.CharField(max_length=128)

class Permission(models.Model):
    name = models.CharField(max_length=128)

class Schedule(models.Model):
    name = models.CharField(max_length=128)

class Fees(models.Model):
    name = models.CharField(max_length=128)

class Duration(models.Model):
    name = models.CharField(max_length=128)

